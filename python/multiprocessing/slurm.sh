#!/bin/bash

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=multiprocessing
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --output=%j.out

module load anaconda/2020.11

python multi.py

