# This Python program is submitted to SLURM via
# the "sbatch slurm.sh"

from multiprocessing import Pool
import multiprocessing
import os

def f(x):
    return x*2

if __name__ == "__main__":

    # cpu_count() will report /all/ CPUs on the node
    # This is not what we should use.
    ncpus = multiprocessing.cpu_count()
    print("detected {} cores".format(ncpus))

    # This will report the number of CPU cores SLURM
    # has allocated us. This is the correct number to
    # pass to Pool()
    try:
        ncpus = int(os.environ['SLURM_JOB_CPUS_PER_NODE'])
        print("my Slurm allocation is {} cores".format(ncpus))
    except KeyError:
        print("Not running under Slurm, setting ncpus to 2")
        ncpus = 2

    pool = Pool(processes=ncpus)
    y_parallel = pool.map(f, range(10)) 
    print(y_parallel)
