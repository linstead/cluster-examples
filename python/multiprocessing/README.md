Python's multiprocessing module can be used for parallelising Python tasks on one node only. It doesn't support multiprocessing across multiple nodes.

For higher levels of parallelism, typically MPI is used for inter-node parallelism, the multiprocessing, dask, joblib or similar for the on-node parallelism. (So-called "hybrid" parallelism)

