from mpi4py import MPI
from joblib import Parallel, delayed
from math import sqrt

comm = MPI.COMM_WORLD
rank = comm.rank  # The process ID (integer 0-3 for 4-process run)

# joblib's 'n_jobs' here must match the --cpus-per-task we've requested from Slurm
# We can find it out at runtime with:
# import os
# n_jobs = os.environ['SLURM_CPUS_PER_TASK']
# Note that $SLURM_CPUS_PER_TASK is only set if --cpus-per-task is used.

data = Parallel(n_jobs=6, prefer="threads", verbose=100)(delayed(sqrt)(i ** 2) for i in range(rank*10, (rank+1)*10))

data = comm.gather(data, root=0)

if rank==0:
  print(data)
