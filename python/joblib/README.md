# Hybrid Python mpi4py + joblib example

This is a simple example of how to configure and run a hybrid MPI and joblib parallelised application on the cluster.

## Setting up the environment

This example was set up with the `anaconda/2020.11` module, though this will likely also work with `anaconda/5.0.0_py3`. To quickly recreate the environment used for this example you can run the following:

```
conda env create -f environment.yaml
```

To create it manually:
```
module load anaconda/2020.11
conda create -c intel -n joblib mpi4py 
conda activate joblib
conda install joblib
```

### Known issues

Some versions of the Intel MPI library packaged with `conda` fail to correctly set the path to the `fi_info` communication helper binary. If you see an error reporting this file is missing during the installation or activation of the conda env, add it to your path thus:

```
export PATH=<path to your env>/bin/libfabric:$PATH
```

Recent versions should not require this step.

## Description of the code

`joblib-with-mpi.py` does a trivial mathematical operation over the elements of a list (`sqrt(i**2)`). Each MPI task (the "outer" level of parallelism) operates on a different segment of the list defined by the "rank" or ID number of the MPI task. Each task is further parallelised using `joblib`'s `Parallel()` function. At the end of processing, MPI `comm.gather()` is used to collect each tasks segment of the computation results into one list.

`slurm.sh` is the submit script for the cluster, and shows how to request the appropriate resources for our simple application.

Hybrid parallelism refers to a mixture of inter-node and intra-node parallelism. Inter-node is typically performed with MPI (mpi4py in Python). Intra-node can either be a distributed-worker scenario or shared-memory threaded (for example OpenMP for C or C++ applications). With Python, the style of intra-node parallelism depends on the module being used, `multiprocessing`, `dask`, `joblib` are common. Each offers different, or multiple configuration options.
