#!/bin/bash

# In the next three lines we set the resources we want.
# We will have 4 MPI tasks (2 nodes with 2 tasks on each)
# and each MPI task (or rank) will get 6 CPUs. 
# joblib on each MPI rank will then be set up to have 
# 6 workers which will get assigned to these per-rank CPU cores.

#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=6
#SBATCH --mail-user=linstead
#SBATCH --mail-type=end

#SBATCH --qos=priority
#SBATCH --job-name=joblib
#SBATCH --output=%x-%J.out
#SBATCH --time=00:05:00

# these things are needed for the newer versions of Intel MPI. Earlier versions of
# Intel MPI used the shm:dapl communication fabric. This has been removed in recent
# versions in favour of shm:ofi, which is the version assumed by `conda`.
# The following lines reset the cluster's default settings in favour of the newer versions.
unset I_MPI_DAPL_UD
unset I_MPI_DAPL_UD_PROVIDER
export I_MPI_FABRICS=shm:ofi

module load anaconda/2020.11
source activate joblib

mpirun -np $SLURM_NTASKS python joblib-with-mpi.py
