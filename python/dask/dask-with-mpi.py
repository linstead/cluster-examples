from mpi4py import MPI
import dask
from dask import delayed
from dask.diagnostics import Profiler
from math import sqrt

#from dask.distributed import Client
#client = Client(n_workers=6)

dask.config.set(scheduler='threads')
dask.config.set(num_workers=6)

comm = MPI.COMM_WORLD
rank = comm.rank

# dask's 'num_workers' here must match the --cpus-per-task we've requested from Slurm
# We can find it out at runtime with:
# import os
# num_workers = os.environ['SLURM_CPUS_PER_TASK']
# Note that $SLURM_CPUS_PER_TASK is only set if --cpus-per-task is used.

data = delayed((sqrt)(i ** 2) for i in range(rank*10, (rank+1)*10))

# optional: use the Profiler to visualise the state of the workers over time.

#with Profiler():
#  data = data.compute()

# setting dask.config.set(num_workers=n) above means all calls to compute() use this
# many workers. We can also call data.compute(num_workers=n)
data = data.compute()

data = comm.gather(data, root=0)

if rank==0:
  print(data)
