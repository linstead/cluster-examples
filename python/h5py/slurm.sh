#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=h5py_test
#SBATCH --output=%x-%j.out
#SBATCH --time=00-00:05:00

# now let's run the same task on 2 nodes, with one task on each
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1  #could also say --ntasks=2 here

export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so

module load anaconda/5.0.0_py3
module load intel/2018.3
#module load hdf5/1.8.21/intel/parallel
source activate parallel

# here we use $SLURM_NTASKS to match the number of copies
# to the number of tasks (CPUs) we requested
srun -n $SLURM_NTASKS python h5py_test.py
