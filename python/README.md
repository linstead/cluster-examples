This is a set of examples for creating and using Python on the cluster, some with mpi4py with the Intel MPI library.

Some examples programs are taken from the `mpi4py` tutorial documentation.

# Setup

## Create an environment

```
conda create -n parallel -c intel mpi4py
```

Use the Intel channel to ensure the Intel MPI library. Otherwise, the MPICH library will be install, which we don't currently support.

## Activate the new environment

```
conda activate parallel
```

## Installing numpy (required for one of the example programs)

```
conda install numpy # uses MKL by default
```

## Installing h5py

Install h5py against our own parallel HDF5 library, this will also install mpi4py correctly

```
module load intel/2018.3
CC="mpiicc" HDF5_MPI="ON" HDF5_DIR="/p/system/packages/hdf5/1.12.0/parallel" pip install --no-binary=h5py h5py
```
