#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=pypar
#SBATCH --account=its
#SBATCH --ntasks=4
#SBATCH --ntasks-per-node=1
#SBATCH --output=%x-%J.out
#SBATCH --time=00:05:00

### BOILERPLATE: You shouldn't need to change anything in this section  ###

# these two variables are set by default on the cluster, but are not
# relevant for newer Intel MPI versions (2020 onwards) which are installed
# via conda
#unset I_MPI_DAPL_UD
#unset I_MPI_DAPL_UD_PROVIDER

# set up fabrics. This is for newer Intel MPI versions (2020 onwards) 
#export I_MPI_FABRICS=shm:ofi
module load intel/2018.3

# Tell Intel MPI which process management interface to use. We use Slurm's.
export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so

### END BOILERPLATE ###

# if we're using "conda activate", we meed to make sure it's init'ed. .initconda.sh
# contains the chunk that "conda init bash" put in .bashrc (which doesn't get sourced 
# by Slurm

source $HOME/.initconda.sh
conda activate parallel

# Run the application via srun.
#srun -n $SLURM_NTASKS python mpi-basic.py
#srun -n $SLURM_NTASKS python mpi-nonblocking.py
#srun -n $SLURM_NTASKS python mpi-numpy.py
#srun -n $SLURM_NTASKS python mpi-io-numpy.py
#srun -n $SLURM_NTASKS python mpi-io-numpy-noncontig.py
srun -n $SLURM_NTASKS python h5py_test.py
