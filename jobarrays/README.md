# Job Arrays

[SLURM job arrays](https://slurm.schedmd.com/job_array.html) offer a mechanism for submitting and managing collections of similar jobs quickly and easily.