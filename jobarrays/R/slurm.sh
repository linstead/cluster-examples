#!/bin/bash

# This script will run a number of identical copies of a program.
# Each copy accesses the environment variable "SLURM_ARRAY_TASKID"
# to perform a different action.
# Output from each copy goes into a separate file ("Rtest-nnnnnn.out" where nnnnnn 
# is the job ID)

# See also:
# https://www.pik-potsdam.de/services/it/hpc/user-guides/slurm/slurm-job-arrays

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=Rtest
#SBATCH --ntasks=1
#SBATCH --output=Rtest-%A-%a.out

# The next line sets up a job array with IDs running from 1860 to 2016
# It limits (with '%') the number of jobs to run concurrently.
# This is useful to prevent, for example, too many programs from competing
# for the same input data file, which is bad if the file is large.
#SBATCH --array=1860-2016%4

export LC_ALL="C"
module load intel/2018.1
module load R/3.4.4
Rscript hello.R
