import os, sys

try:
    task = os.environ['SLURM_ARRAY_TASK_ID']
except KeyError:
    print "Not running with SLURM job arrays"
    sys.exit(1)

#clims=["HadGEM2-ES","MIROC-ESM-CHEM","GFDL-ESM2M","IPSL-CM5A-LR","NorESM1-M"] 
#clim=clims[int(task)]
#print "running %s on %s" % (clim, os.environ['HOSTNAME'])

print "running task %s on %s" % (task, os.environ['HOSTNAME'])
