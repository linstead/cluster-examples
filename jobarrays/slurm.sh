#!/bin/bash

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=demo_jobarrays
#SBATCH --account=its

## Set up 5 jobs in array, numbered 0-4
## with a maximum of 1 simultaneously running
## task (%n should match nodes*ntasks-per-node)
##SBATCH --nodes=1
##SBATCH --ntasks-per-node=1
##SBATCH --array=0-4%1

## Alteratively, use my entire allocation. 
## Here, it's 32 CPUs over two nodes:
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=16
#SBATCH --array=0-1023%32

## Each task in the job array can send it's output to a
## different file
#SBATCH --output=jobarray-%A_%a.out

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "------------------------------------------------------------"

module load anaconda/5.0.0
python $HOME/cluster-examples/jobarrays/echo.py
