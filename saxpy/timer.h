#ifndef TIMER_H
#define TIMER_H

#include <sys/types.h>
#include <time.h>
int timer_s() {
    time_t t;
    time(&t);

    return (int) t;
}

double timer() {
    clock_t t;
    t = clock();

    return (double) t / CLOCKS_PER_SEC;
}
#endif /* TIMER_H */
