#!/bin/bash

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=saxpy
#SBATCH --ntasks=1
#SBATCH --account=its
#SBATCH --time=00:05:00

#echo "gcc O3"
#$HOME/cluster-examples/saxpy/saxpy.gccO3
echo "gcc Ofast"
$HOME/cluster-examples/saxpy/saxpy.gccfast
#echo "icc O3 precise"
#$HOME/cluster-examples/saxpy/saxpy.iccO3precise
#echo "icc O3"
#$HOME/cluster-examples/saxpy/saxpy.iccO3

