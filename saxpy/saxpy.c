#include <stdio.h>
#include <stdlib.h>
#include "timer.h"

#define SMALL 5
#define BIG 1<<29
#define NLOOPS 1

void saxpy(int n, float a, float *x, float *y) {

    for (int i=0; i<n; i++) {
        y[i] = a*x[i] + y[i];
        //if (n==SMALL) {
        //    printf("result:%f\n", y[i]);
        //}
    }
}

int main(int argc, char **argv) {

    double t0, t1;

    int n=BIG;
    //int n=SMALL;
    
    float *x = (float *)malloc(n * sizeof(float));
    float *y = (float *)malloc(n * sizeof(float));

    if (n==SMALL) {
        x[0]=3.0; y[0]=9.0; x[1]=4.0; y[1]=3.0; x[2]=7.0; y[2]=0.0; x[3]=0.0; y[3]=0.0; x[4]=1.0; y[4]=12.0;
    }

    for (int i=0; i<n; i++) {
        x[i] = 10.0*(float)random()/(float)RAND_MAX;
        y[i] = 10.0*(float)random()/(float)RAND_MAX;
        if (n==SMALL) {
            printf("x:%f y:%f \n", x[i], y[i]);
        }
    }

    t0 = timer();
    for (int i=0; i<NLOOPS; i++) {
        saxpy(n, 2.0, x, y);
        //saxpy(n, 2.0*(float)random()/(float)RAND_MAX, x, y);
    }
    t1 = timer();
    printf("time for %d calls: %f sec\n", NLOOPS, (t1-t0));
    printf("%d\n", BIG);
    //printf("Average time per call: %f sec\n", (t1-t0)/NLOOPS);
    printf("size of vector: %zu (%d)\n", (n*sizeof(float)), n);

/*    for (int i=0; i<NLOOPS; i++) {
        t0 = timer();
        saxpy(n, 2.0, x, y);
        t1 = timer();
        printf("call: %d time: %f sec\n", i, (t1-t0)/NLOOPS);
        //saxpy(n, 2.0*(float)random()/(float)RAND_MAX, x, y);
    }
*/
    free(x); free(y);
    return 0;
}
