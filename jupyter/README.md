**This is an advanced guide and may require some debugging effort on your part if it doesn't work as expected. I can only offer limited support.**

# Starting a Jupyter notebook on a cluster compute node and connecting from a client PC or laptop

## Introduction 

This example demonstrates how to run a Jupyter notebook remotely on the cluster, and use a local web browser on your PC or laptop to interact with it. 

Jupyter notebooks on the cluster have been tested with Python 3, R 3.5.1 and Julia 1.0.0.

## Before you begin

Check that you can do the following before proceeding:
  - open an SSH connection to the cluster (e.g. `ssh <your username>@cluster.pik-potsdam.de`). See the [documentation](https://www.pik-potsdam.de/en/institute/about/it-services/hpc/user-guides/access) for details.
  - SSH from one cluster node to another (e.g. on `login01` run `ssh login02` and _vice versa_.)
    - if this doesn't work, ensure the contents of your public key (usually in `$HOME/.ssh/id_rsa.pub`) are in your authorized keys file (usually `$HOME/.ssh/authorized_keys`), on the cluster.

See the _Troubleshooting_ section below if you're still having problems.

## Startup

To execute these steps and connect to your Jupyter notebook, do the following:
- First, **on the cluster**, run `./startnotebook.sh`

- output similar to the following will be printed in your terminal:

```
Waiting for notebook to start /
1) Tunnel to the cluster from your laptop: run this command on your laptop:
    ssh -L8888:localhost:18646 linstead@login01.pik-potsdam.de -N
2) Then point your browser to:
    http://localhost:8888
If prompted for a token, check 'jupyter notebook list'
If something has gone wrong, check /home/linstead/cluster-examples/jupyter/jupyter-23923651.err
If port 8888 is already in use on your laptop, you can change to another number
in both the ssh tunnel command and the 'localhost:nnnn' address
Close the notebook with:
    scancel 23923651
```
  
- Second, **on your laptop** run the SSH tunnel command given by 1)
- Finally, also **on your laptop**, point your browser to the address in 2) above. 


### Two ways to authenticate your notebook:

1) Set a password for all your Jupyter notebooks on the cluster:
    
- login to the cluster
- run the following in a the terminal (having loaded the appropriate Anaconda module) and follow the instructions. You only need to do this once, and it applies to all of your notebooks on the cluster.
    
    ```
    jupyter-notebook password
    ```
    
Point your browser URL in the output above (note: Usually `http://localhost:8888`. You will be prompted for the password you just set.

> note that the hashed password is read on notebook startup, so if you change or set the password you'll need to restart the notebook.

2) Alternatively, if prompted for a token, run this on the cluster:

  ```
  jupyter notebook list
  ```
and extract the token from the URL for the notebook. 

### Changing defaults

The default setting for the notebook is 1 CPU core. If you would like to use more, set `--cpus-per-task` in `slurm.sh` accordingly.

To use one of the NVidia GPUs, set the QOS appropriately and add:
```
#SBATCH --partition=gpu
#SBATCH --gres=gpu:v100:1
```
You can edit the `slurm.sh` file to request whichever resources you need for your notebook. Be aware that as long as the notebook is active, these resources are unavailable to other users, as usual with Slurm jobs.

## Shutdown

To shut down your notebook completely, simply cancel the SLURM job on the cluster:

```scancel <jobid>```

`<jobid>` can be found with `squeue` or in the output from the `startnotebook.sh` script.

# Jupyter notebooks for Julia

## Preparation

```
using Pkg
Pkg.add("IJulia")
```

# Jupyter notebooks for R

## Preparation

Running R notebooks in Jupyter requires a one-off setup of your R installation on the cluster.

In the file `~/.R/Makevars`, set the following:
```
CC=icc -std=gnu11
```
(this is required to install 'uuid', a dependency of the 'IRkernel' package we are going to install)

```
module load anaconda/2020.11 # this is where the Jupyter client comes from
module load R/3.5.1 # or whichever version you prefer
module load intel/2018.1 # if the R version needs it. (yes, for 3.4.4 and 3.5.1)
```

1. install.packages("IRkernel")
2. IRkernel::installspec()

see https://irkernel.github.io/installation/

Now when you start your session, there will be an option to create a new R notebook. Be sure to load an R module in your submission script.

# Things to note
- The notebook stays running for as long as the SLURM job is active, regardless of 
whether you are using the notebook for computation. The resources requested in the 
submission are unavailable to other users during this time. Please keep this mind if the 
cluster is busy and other users are waiting for resources. Use `scancel` to shut down the notebook when
you're done.
- notebooks, output files, graphics etc. are saved on the cluster, not on your local machine.
- For each conda env to appear in the drop-down list in the Jupyter interface, it needs to have the `ipykernel` module installed.
 
# Troubleshooting

Here is a list of common errors and their possible solutions:

- "Waiting for notebook to start" takes a long time
  - this phase depends on how fast a Slurm job can be started and how fast the notebook startup itself is. Check with `squeue` on the cluster if your job has entered the Running state or is still Pending. If the cluster is busy this will take more time.
- Browser reports "Unable to connect" or "Site can't be reached" when connecting to localhost:8888
  - check that the laptop-to-cluster tunnel has been set up. A common mistake is to run the `ssh -L8888...` line on the cluster instead of on your laptop.
- __Permission denied (publickey)__
  - Check that you can SSH from one login node to another (without entering a passphrase). If not, create a new default SSH key (blank passphrase) on the cluster: `ssh-keygen` and accept the default name. Caution: if you already have a file `.ssh/id_rsa` this will overwrite your key. In this case simply add the contents of `is_rsa.pub` to your `$HOME/.ssh/authorized_keys` file. You can also create a custom key (i.e. with a non-default name): `ssh-keygen -f $HOME/.ssh/id_rsa_c2c`. If you want to use a non-default key, you will need to adjust your `.ssh/config` file:
```
Host login*
  IdentityFile ~/.ssh/id_rsa_c2c
Host cs-*
  IdentityFile ~/.ssh/id_rsa_c2c
```
  
In either case, add the contents of the `.pub` part of the key to your `$HOME/.ssh/authorized_keys` file.
- __Address already in use__
  - You try to set up the laptop-cluster tunnel with `ssh -L8888...` but get the following message:
  ```
     bind [127.0.0.1]:8888: Address already in use
     channel_setup_fwd_listener_tcpip: cannot listen to port: 8888
     Could not request local forwarding.
  ```
  This means port 8888 is already being used by another application on your laptop or another tunnel. The easiest workaround is to change `8888` in two places: e.g. `ssh -L9999...` command and then in the address you browse to `localhost:9999`. This applies also if you want to run **more than one Jupyter notebook** at a time.

- Connection to notebook in the browser is lost.
  - Check that the Slurm job which controls the notebook is still running on the cluster (`squeue`). If not, you will need to run `startnotebook.sh` again. If you see `[C 09:58:17.602 NotebookApp] received signal 15, stopping`, this means that the time-limit specified in `slurm.sh` was reached and the job was cancelled by Slurm. You can increase the time-limit in `slurm.sh` up to the limit set by the QOS you submit to.

# Technical background

The scripts here run a few steps, which will be explained in more detail here.
 
a) Start a Jupyter notebook on a compute node.

b) Set up a unique connection from the compute node back to the submitting host (`login01.pik-potsdam.de` or `login02.pik-potsdam.de`). This sends all outgoing traffic from the Jupyter notebook port back to the login node.

c) Set up a unique connection from your laptop to the port on the login node receiving the Notebook traffic from step b).

d) Open the remote Jupyter notebook with your browser of choice.

Steps a) and d) are mostly self-explanatory. Steps b) and c) establish a connection in two hops, 
originating at the start point (your laptop) and end point (the compute node) we want to connect and meeting in the middle (on the login node).

These are done by means of secure shell (SSH) tunnels. When the two connections are joined, we
can connect to the remote (compute node) end as if it were a local connection on our own
computer.
