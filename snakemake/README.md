# Snakemake on the cluster

## Load required modules

```
module load anaconda/2020.07 # this is our base Python installation
module load snakemake/6.0.5
```

## Create a conda env for your workflow

For example, from the Snakemake tutorial:

```
conda create -n snakemake-tutorial --file environment.yaml
```

`environment.yaml` in this directory is slightly different to the official Snakemake tutorial. In ours, we exclude `snakemake` itself, since we've provided it as a module.

## Choose a Slurm profile

To run your workflow via Slurm jobs, you can choose one of four predefined profiles. The profile and their limits are:

| Profile name     | ntasks | CPUs per task | QOS        | Timelimit | Node type             |
| ---------------- | ------ | ------------- | ---------- | --------- | --------------------- |   
| `slurm_priority` | 1      | 16            | `priority` | 24 hours  | `haswell` (16 core)   |
| `slurm_short16`  | 1      | 16            | `short`    | 24 hours  | `haswell` (16 core)   |
| `slurm_short32`  | 1      | 32            | `short`    | 24 hours  | `broadwell` (32 core) |
| `slurm_medium`   | 1      | 16            | `medium`   | 7 days    | `haswell` (16 core)   |


### Override Slurm defaults

You can override the Slurm defaults with a `resources` stanza in your Snakefile, e.g. to reduce the timelimit to 1.5 hours:

```
resources:
  time="01:30:00"
```

## Run your workflow with a specific profile

```
snakemake --profile=slurm_priority 
```


## Creating your own Slurm profile:

If you want to create your own Slurm profile, you can use `cookiecutter` (included in the `snakemake/6.0.5` module)

```
cookiecutter https://github.com/Snakemake-Profiles/slurm.git
```

You can use the arguments from an existing profile as a template for your own, e.g. 
`/p/system/packages/snakemake/xdg/snakemake/slurm_priority/settings.json`

In order for Snakemake to find this profile, add the path to your `XDG_CONFIG_DIRS` environment variable, if it's not already in `$HOME/.config/snakemake`
