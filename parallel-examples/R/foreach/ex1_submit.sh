#!/bin/bash

#SBATCH --job-name=clusterApply
#SBATCH --qos=priority
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
##SBATCH --constraint=broadwell
#SBATCH --output=std_%j.out
#SBATCH --error=std_%j.err
#SBATCH --exclusive
#SBATCH --time=00:10:00
echo "---------------------"
echo "SLURM JOB ID: " $SLURM_JOBID
echo "$SLURM_NTASKS tasks"
echo "$SLURM_NTASKS_PER_NODE tasks_per_node"
echo "$SLURM_JOB_CPUS_PER_NODE job_cpus_per_node"
echo "Running on nodes: $SLURM_NODELIST"
echo "----------------------"
module purge
module load intel/2018.1
module load R/3.4.4

srun -n 1 R CMD BATCH --no-save ex1_slurm_foreach.R

