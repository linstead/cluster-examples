suppressMessages(library(doMPI))

cl <- startMPIcluster(verbose=TRUE, comm=0) # by default will start one fewer slave
                        # than elements in tasks requested (to account for master)
registerDoMPI(cl)

mpiopts <- list(chunkSize=10)

#print( c("universe size", mpi.universe.size() ))

results <- foreach(i = 1:5000, .options.mpi=mpiopts) %dopar% {
      out = mean(rnorm(1e6))
}

closeCluster(cl)
mpi.quit()
