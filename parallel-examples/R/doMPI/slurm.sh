#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=doMPI_example
#SBATCH --account=its
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
##SBATCH --constraint=broadwell
#SBATCH --time=00:05:00

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "$SLURM_NTASKS_PER_NODE tasks per node"
echo "Running on nodes: $SLURM_NODELIST"
echo "------------------------------------------------------------"

module purge
module load intel/2018.3
module load R/3.4.4

###
# This section for running via srun. Initial experiments with simple code seems
# to indicate that the tasks are unevenly distributed to ranks with this method.
###

# srun requires SLURM's process management interface (libpmi)
#export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so
#srun -v -n $SLURM_NTASKS R --slave -f ex_dompi_simple.R
#srun -v -n $SLURM_NTASKS R --slave -f plots.R
#srun -v -n $SLURM_NTASKS R --slave -f helloWorld.R
#srun -v -n $SLURM_NTASKS R --slave -f initEnvir.R

###
# This section for running using mpirun. We must use a machinefile to correctly map
# allocated cores/nodes to the mpirun environment.
###

MACHINEFILE="nodes.$SLURM_JOB_ID"

# Generate Machinefile for mpi such that hosts are in the same
# order as if run via srun

srun -l /bin/hostname | sort -n | awk '{print $2}' > $MACHINEFILE

# Run using generated Machine file:
unset I_MPI_PMI_LIBRARY
# MPI spawning (with mpirun -np 1) seems to not work (with -machinefile? ever?)
I_MPI_DEBUG=1 mpirun -np $SLURM_NTASKS -machinefile $MACHINEFILE R --slave -f ex_dompi_simple.R
rm $MACHINEFILE




