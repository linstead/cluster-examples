# Parallel Examples

This is a collection of short programs demonstrating different forms of parallelism in different languages.

- [c/serial](c/serial)

    Language: C. A baseline, non-parallel version to start with.

- [c/openmp](c/openmp)

    Language: C. Thread-based parallelism with OpenMP. Set number of threads with OMP_NUM_THREADS

- [c/mpi](c/mpi)

    Language: C. Message Passing Interface (MPI) example. Requires an MPI library and compiler (e.g. OpenMPI, MPICH2, Intel MPI)
