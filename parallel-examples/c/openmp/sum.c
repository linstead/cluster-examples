/*
 * Compute sum of integers up to n. OpenMP threaded version.
 * Compile with:
 *
 *     gcc -Wall -march=haswell -mtune=haswell -O3 -fopenmp -o sum sum.c
 *
 * Control the number of threads with "export OMP_NUM_THREADS=n" before running
 * where n is the number of CPU cores you have available (for best performance)
 *
 * Run with, for example:
 * time ./sum 123456789123
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    unsigned long long int sum = 0;
    unsigned long long int n;
    unsigned long long int i;

    if (argc == 2) {
        n = atoll(argv[1]);
        printf("Summing integers from 0 to %llu (inclusive)\n", n);
    } else {
        printf("Usage: %s <number to sum to>\n", argv[0]);
        exit(1);
    }

    // The following line is a directive for the compiler, making the for-loop
    // parallel threaded. We also need to tell the compiler that "sum" will 
    // be a _reduction_ of all the thread-local partial sums.

    #pragma omp parallel for reduction(+:sum)
    for (i=0; i<=n; i++) {
        sum += i;
    }

    printf("The sum is: %llu\n", sum);

    return 0;
}
