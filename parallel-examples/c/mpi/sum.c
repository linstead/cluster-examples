/*
 * Compute sum of integers in the range [0,n). (up to but not including n)
 *
 * MPI parallel version
 *
 * Compile with:
 *      module load intel/2018.3 # we're using the Intel MPI library
 *      mpicc -Wall -O3 -march=haswell -mtune=haswell -o sum sum.c
 *
 * Run with, for example:
 *      time mpirun -np 4 ./sum 123456789123
 *
 * mpirun starts the right number of copies of this program and sets up the
 * communication environment for them.
 *
 * Note: on a cluster login node do this before running:
 *      export I_MPI_FABRICS=shm:shm
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]) {
    unsigned long long int localsum = 0;
    unsigned long long int n;
    unsigned long long int j, k, i;
    unsigned long long int *globalsum = 0;

    unsigned long long int chunksize;
    int rank, numprocs;

    /* Fetch 'n' from the command line arguments to the program */
    if (argc == 2) {
        n = atoll(argv[1]);
    } else {
        exit(1);
    }

    /* Initialise MPI */
    MPI_Init(&argc, &argv);
    /* Get the total number of participating processes (into 'numprocs') */
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    /* Get the ID/number of the current process into 'rank' */
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* Initialise 'globalsum' on rank 0 only.
     * The same code runs on all ranks (processors), so if we need to do different
     * things on different processors, we need to examine 'rank' */
    if (rank == 0) {
        globalsum = (unsigned long long int *) malloc (sizeof(unsigned long long int));
    }

    /* Every rank is doing the same calculation of "chunksize" here. We could just 
     * calculate this on rank 0 and use MPI to send the value to all the other ranks.
     * This way is simpler for now and doesn't introduce much of a performance problem */
    chunksize = n / numprocs;

    /* Set up j (lower bound) and k (upper bound) of the summation: they depend on my rank */
    j = rank * chunksize;
    k = j + (chunksize-1);

    /* On the last rank, we might have a bigger chunk (because n/numprocs rounds down)
     * so extend k into the final chunk */
    if (rank == numprocs-1) { 
        //k += n - (numprocs * chunksize);
        // or more simply:
        k = n-1;
    }

    /* Loop from the start to the end of the chunk, summing as we go. */
    localsum=0;
    for (i=j; i<=k; i++) {
        localsum += i;
    }
    /* Print some rank-local information */
    printf("Rank %d: local sum: %llu(lower: %llu, upper: %llu)\n", rank, localsum, j ,k);

    /* 
     * Collect and Reduce the local sums onto rank 0, summing them with MPI_SUM 
     * into 'globalsum'. This happens on all ranks.
     */
    MPI_Reduce(&localsum, globalsum, 1, MPI_LONG_LONG_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    /* Print the result, but only on rank 0 - since this is where the reduction came */
    if (rank == 0) {
        printf("***************************************\n");
        printf("* Sum to %llu: %llu *\n", n, *globalsum);
        printf("***************************************\n");
    }

    /* Clean up all ranks */
    MPI_Finalize();

    /* Free up memory, but since it was allocated only on rank 0, only free on rank 0 */
    if (rank == 0)
        free(globalsum);

    return 0;
}
