/*
 * Compute sum of integers in the range [0,n). (up to but not including n)
 *
 * Serial version
 *
 * Compile with:
 * gcc -Wall -march=haswell -mtune=haswell -O3 -o sum sum.c
 *
 * Run with, for example:
 * time ./sum 123456789123
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    unsigned long long int sum = 0;
    unsigned long long int n;
    int i;

    if (argc == 2) {
        n = atoll(argv[1]);
        printf("Summing integers in the range [0,%llu)\n", n);
    } else {
        printf("Usage: %s <number to sum to>\n", argv[0]);
        exit(1);
    }

    for (i=0; i<n; i++) {
        sum += i;
    }

    printf("The sum is: %llu\n", sum);

    return 0;
}
