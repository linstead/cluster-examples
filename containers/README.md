## Load dependencies

```
~$ module load singularity
*** EXPERIMENTAL ***
*** Use Spack to load dependencies:
***  source /p/system/packages/spack/share/spack/setup-env.sh 
***  spack load squashfs@4.4%gcc@8.3.0 
*** Build Singularity images on a system where you have root (not here!), or use --remote
*** EXPERIMENTAL ***

~$ source /p/system/packages/spack/share/spack/setup-env.sh
~$ spack load squashfs@4.4%gcc@8.3.0
```

## pull and convert a Docker image:

```
~$ singularity pull hello-world.sif docker://hello-world:latest
INFO:    Converting OCI blobs to SIF format
INFO:    Starting build...
Getting image source signatures
Copying blob b8dfde127a29 done  
Copying config 1189c90721 done  
Writing manifest to image destination
Storing signatures
2021/03/15 12:16:07  info unpack layer: sha256:b8dfde127a2919ff59ad3fd4a0776de178a555a76fff77a506e128aea3ed41e3
INFO:    Creating SIF file...
```
