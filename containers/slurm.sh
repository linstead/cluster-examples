#!/bin/bash

#SBATCH --job-name=docker2singularity
#SBATCH --qos=short
#SBATCH --time=00:05:00
#SBATCH --nodes=1

module load singularity
source /p/system/packages/spack/share/spack/setup-env.sh
spack load squashfs@4.4%gcc@8.3.0

# create a uniquely-named temporary directory, by default under /tmp
mytmpdir=/tmp/singularity
mkdir -p $mytmpdir/mnt/session

# define a cleanup function to be called at the end of the run
cleanup() {
  rm -r $mytmpdir
  exit
}

# By trapping the EXIT signal, we also account for cases where the job 
# ends abnormally (e.g. is cancelled)
trap cleanup EXIT


singularity run file-out.sif 
