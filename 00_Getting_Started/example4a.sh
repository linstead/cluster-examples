#!/bin/bash

#SBATCH --job-name=example4a
#SBATCH --output=%x-%j.out

# This time, instead of running 2 tasks (possibly) on one node, 
# we specify to Slurm that we want to run 1 task on each of 2 nodes:
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1

#SBATCH --time=00-00:05:00

# run multiple copies of the command. Each gets its own CPU core on a different node.
srun -n 2 hostname

# The output from this script is now something like:
# cs-f14c05b04
# cs-f14c05b05
