# Getting started with Slurm on the PIK Cluster

Slurm is the workload scheduling software that you use to place your programs in the queue for execution on the cluster's compute nodes.

This subdirectory contains a series of examples of submission scripts showing the most important parameters you need to set.

Scripts are submitted with `sbatch <scriptname>`

You can see what's currently in the queue with `squeue` (`squeue -u <your username>` to see just your running jobs). Most of these examples will be queued and run very quickly, so you may well not see any of them with `squeue`

Use `sacct` to see your completed jobs (since midnight)

For more information on these commands, see:
- `man sbatch`
- `man squeue`
- `man sacct`
