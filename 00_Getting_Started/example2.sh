#!/bin/bash

# Lines starting with '#SBATCH' are read by SLURM as parameters for the run.
# Other lines starting with '#', like this one, are comments and ignored by 
# the shell and by SLURM.

# Let's set the name of the job. Useful if we're running many scripts and would
# like to see at a glance which is which
#SBATCH --job-name=myrun

# Instead of using the filename 'slurm-<jobid>.out' to save output, let's
# use the jobname we defined above, plus the job ID number
#SBATCH --output=%x-%j.out

# run one copy of 'hostname'
hostname
