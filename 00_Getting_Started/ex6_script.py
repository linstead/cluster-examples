import os
import subprocess

# set up a subprocess to call the "hostname" command
p = subprocess.Popen("hostname", stdout=subprocess.PIPE)

# run the command, saving the output
(output,error)=p.communicate()

# print a message
print("My process ID is", os.getpid(), "on node", output.decode().strip())
