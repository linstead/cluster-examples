#!/bin/bash

#SBATCH --job-name=example7
#SBATCH --output=%x-%j.out
#SBATCH --time=00-00:05:00

#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=7G #7GB per cpu core

module load anaconda/5.0.0_py3

# Run the Python program. In this example, each process has about
# twice as much memory as standard, since we set --mem-per-cpu above.
srun -n $SLURM_NTASKS python ex6_script.py
