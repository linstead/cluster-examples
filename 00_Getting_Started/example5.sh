#!/bin/bash

#SBATCH --job-name=example5
#SBATCH --output=%x-%j.out
#SBATCH --ntasks=1
#SBATCH --time=00-00:05:00

# Run a Python program

# We can do other things in this script, like load modules
# we need for particular software
module load anaconda/5.0.0_py3
python -c "import os; print(os.environ['HOSTNAME'])"

# More typically we'd run a Python script:
# python myscript.py
