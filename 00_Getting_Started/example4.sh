#!/bin/bash

#SBATCH --job-name=example4
#SBATCH --output=%x-%j.out
#SBATCH --ntasks=2

#SBATCH --time=00-00:05:00

# The previous example only ran one copy of the program `hostname` even though we asked for two tasks.
# Slurm needs to be told to do something different.

# run multiple copies of the command. Each gets its own CPU core.
srun -n 2 hostname

# The output from this script is now something like:
# cs-f14c05b04
# cs-f14c05b04
# 'srun' is used by SLURM to start as many copies of the program as required.
# The srun parameter '-n' should usually match '--ntasks'
