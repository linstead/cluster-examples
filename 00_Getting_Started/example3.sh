#!/bin/bash

#SBATCH --job-name=example3
#SBATCH --output=%x-%j.out

# Set the number of tasks to 2. SLURM will choose where to run 
# these tasks, which could be all on one node, or across different nodes.
#SBATCH --ntasks=2

# Set the account, or project. Change 'its' to match a project you belong to.
# See 'sacctmgr show assoc where user=$USER format=account' for a list.
#SBATCH --account=its

# Set a timelimit for the run, in this case 5 minutes.
# This helps the scheduler fit your job into the queue more quickly
#SBATCH --time=00-00:05:00

# run the command
hostname

# Even though we set the number of tasks to 2, our output file still
# only contains one entry. Why?
