#!/bin/bash

#SBATCH --job-name=example6
#SBATCH --output=%x-%j.out
#SBATCH --time=00-00:05:00

# now let's run the same task on 2 nodes, with one task on each
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1  #could also say --ntasks=2 here

# Run the Python program
module load anaconda/5.0.0_py3

# here, instead of saying "srun -n 2" we use $SLURM_NTASKS to match 'n'
# to the number of tasks (CPUs) we requested. This is good practice for 
# avoid errors in requested vs expected resources.
srun -n $SLURM_NTASKS python ex6_script.py
