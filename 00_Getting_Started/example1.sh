#!/bin/bash

# The simplest of all SLURM submission scripts.
# Submit with "sbatch ex1.sh".
# All parameters are set to their defaults:
# one CPU, your default user account, short time limit.

# We'll run the following command on the first (and in this case, only)
# task in the allocation of resources SLURM gives us.
hostname

# The output will be in a file called 'slurm-<jobid>.out' and will
# look something like:
# 'cs-f14c05b04'
# i.e. the hostname of the server the script ran on.
