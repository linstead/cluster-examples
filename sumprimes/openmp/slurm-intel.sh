#!/bin/bash

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=intel-sumprimes
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --output=%x-%j.out
#SBATCH --time=01:00:00

module load intel/2018.3
srun -n1 ./sumprimes.intel 1 100000000
