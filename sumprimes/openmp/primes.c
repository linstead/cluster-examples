#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "primes.h"
#include "../util/timer.h"

/* this example uses OpenMP instead of MPI. This introduces thread-level parallelism
 * via the 'omp' #pragmas surrounding the parallelisation 'hotspots' */

double t0, t1;
int tid;
long long unsigned int myu;

#pragma omp threadprivate(t0, t1, tid, myu)

int isprime(int n) {
    double r;
    int f;

    if (n <= 1) return FALSE;
    else if (n < 4) return TRUE;
    else if (n % 2 == 0) return FALSE;
    else if (n < 9) return TRUE; // we have already excluded 4,6 and 8.
    else if (n % 3 == 0) return FALSE;
    else {
        r = floor( sqrt(n) );
        f = 5;
        while (f <= r) {
            if (n % f == 0) return FALSE; //(and step out of the function)
            if (n % (f+2) == 0) return FALSE; //(and step out of the function)
            f += 6;
        }
        return TRUE;
    }
}

long long int sumprimes(int lower, int upper) {
    long long int sum = 0;
    int np = 0;
    int i;

    #pragma omp parallel
    {
    t0 = timer();
    }

    #pragma omp parallel for reduction(+:sum) reduction (+:np)
    for (i=lower; i<=upper; i++) {
        myu = i;
        if (isprime(i)) {
            sum += i;
            np++;
        }
    }

    #pragma omp parallel
    {
    t1 = timer();
    tid = omp_get_thread_num();
    printf("Thread %d took %f seconds. (upper=%llu)\n", tid, t1-t0, myu);
    }

    printf("Number of primes: %d\n", np);

    return sum;
}

