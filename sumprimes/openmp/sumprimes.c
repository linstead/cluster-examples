#include <stdio.h>
#include <stdlib.h>
#include "primes.h"
#include "../util/timer.h"

int main(int argc, char *argv[]) {
    long long int sum = 0;
    int l, u;
    double t0, t1;

    if (argc == 3) {
        l = atoi(argv[1]);
        u = atoi(argv[2]);
        printf("Summing primes between %d and %d\n", l, u);
    } else {
        printf("Usage %s lower upper\n", argv[0]);
        exit(1);
    }

    t0 = timer();
    sum = sumprimes(l, u);
    t1 = timer();

    printf("Sum of primes: %lld\n", sum);
    printf("Time elapsed: %f sec\n", t1-t0);

    return 0;
}
