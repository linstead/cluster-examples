# sumprimes

`sumprimes` is a program (or rather a collection of programs) to demonstrate parallelism with a more complex example.

The purpose of the program is to calculate and sum the prime numbers between two bounds, e.g. (serial version):

```
$ ./sumprimes 0 20
Summing primes between 0 and 20
Number of primes: 8
Sum of primes: 77
```

- [serial](sumprimes/serial)

    A single-CPU version

- [mpi](sumprimes/mpi)

    An MPI-parallel implementation

- [openmp](sumprimes/openmp)

    An OpenMP threaded version