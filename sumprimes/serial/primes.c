#include <stdio.h>
#include <math.h>
#include "primes.h"

/* fast algorithm for determining prime-ness */

int isprime(int n) {
    double r;
    int f;

    if (n <= 1) return FALSE;
    else if (n < 4) return TRUE;
    else if (n % 2 == 0) return FALSE;
    else if (n < 9) return TRUE; // we have already excluded 4,6 and 8.
    else if (n % 3 == 0) return FALSE;
    else {
        r = floor( sqrt(n) );
        f = 5;
        while (f <= r) {
            if (n % f == 0) return FALSE; //(and step out of the function)
            if (n % (f+2) == 0) return FALSE; //(and step out of the function)
            f += 6;
        }
        return TRUE;
    }
}

long long int sumprimes(int lower, int upper) {
    long long int sum = 0;
    int np = 0;
    int i;

    for (i=lower; i<=upper; i++) {
        if (isprime(i)) {
            sum += i;
            np++;
        }
    }

    printf("Number of primes: %d\n", np);

    return sum;
}

