#ifdef MPIPRIMES
/* Use MPI timer functions */

#include "timer.h"
#include "mpi.h"
double timer() {
    return MPI_Wtime();
}

#elif OPENMPPRIMES
#include <sys/types.h>
#include <time.h>
#include <stdio.h>
#include "omp.h"
#include "timer.h"

double timer() {
    double t;
    t = omp_get_wtime();

    return t;
}

#else

#include <sys/types.h>
#include <time.h>
#include <stdio.h>
#include "timer.h"
int timer_s() {
    time_t t;
    time(&t);

    return (int) t;
}

double timer() {
    clock_t t;
    t = clock();

    //printf("clock: %f\n", (double)t);

    return (double) t / CLOCKS_PER_SEC;
}
#endif
