#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "primes.h"
#include "../util/timer.h"

int main(int argc, char *argv[]) {
    unsigned long long int localsum = 0;
    int lower, upper;
    unsigned long long int *globalsum;

    int max, chunksize, range;
    int rank, numprocs;
    double t0, t1;

    /* check that we have enough command line arguments */
    if (argc == 3) {
        lower = atoi(argv[1]);
        upper = atoi(argv[2]);
    } else {
        exit(1);
    }

    /* initialise MPI, get the total number of participating processes and
     * the ID (rank) of the current process. */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* initialise globalsum on rank 0 only.
     * The same code runs on all ranks (processes), so if we need to do different
     * things on different processes, we need to examine 'rank' */

    if (rank == 0)
        globalsum = (unsigned long long int *) malloc (sizeof(long long int));

    /* Split the data across the ranks, i.e. each rank calculates it's own
     * 'lower' and 'upper'. We could also calculate the bounds on rank 0 and
     * use MPI_Send to pass them to the other processes. This is left as a
     * exercise for the reader.
     */

    max = upper;
    range = upper - lower;
    chunksize = range / numprocs;

    /* modify lower and upper: they depend on my rank */
    lower = lower + (rank * (chunksize+1));
    upper = lower + chunksize;

    if (upper > max)
        upper = max;

    /* timer START */
    MPI_Barrier(MPI_COMM_WORLD);
    t0 = timer();

    localsum = sumprimes(lower, upper);

    /* timer END */
    MPI_Barrier(MPI_COMM_WORLD);
    t1 = timer();

    printf("Rank %d: local sum of primes: %llu\n", rank, localsum);

    /* 
     * Reduce the local sums onto rank 0, summing them with MPI_SUM into 'globalsum'
     */

    MPI_Reduce(&localsum, globalsum, 1, MPI_LONG_LONG_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    /* print the result, but only on rank 0 - since this is where the reduction came */
    if (rank == 0) {
        printf("***************************************\n");
        printf("* Sum of primes: %20llu *\n", *globalsum);
        printf("***************************************\n");
    }

    printf("Rank %d: time elapsed: %f seconds.\n", rank, t1-t0);

    /* clean up */
    MPI_Finalize();

    if (rank == 0)
        free(globalsum);

    return 0;
}
