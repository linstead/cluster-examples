#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=sumprimes
#SBATCH --account=its
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --time=00:05:00

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "$SLURM_NTASKS_PER_NODE tasks per node"
echo "Running on nodes: $SLURM_NODELIST"
echo "------------------------------------------------------------"

module purge

# no modulefile yet, but we can source the Intel vars files:
source /p/system/packages/intel/oneapi/2021.1.1/setvars.sh

# To run on a login node, uncomment this line
#     export I_MPI_FABRICS=shm:shm
# To run on a compute node, the following are needed due to a change in the
# underlying communication libraries used by Intel:
export I_MPI_FABRICS=shm:ofi
export FI_PROVIDER=mlx

## srun version (requires SLURM's process management interface (libpmi) to srun)
export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so
srun -n $SLURM_NTASKS $PWD/sumprimes 0 1000000000

## mpirun version
## First, comment out the I_MPI_PMI_LIBRARY and srun lines above.
#echo SLURM JOB NODELIST   : $SLURM_JOB_NODELIST
#echo SLURM TASKS PER NODE : $SLURM_TASKS_PER_NODE
#I_MPI_DEBUG=5 mpirun -bootstrap slurm -n $SLURM_NTASKS $HOME/cluster-examples/sumprimes/mpi/sumprimes 0 1000000000
