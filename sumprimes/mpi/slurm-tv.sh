#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=sumprimes
#SBATCH --account=its
#SBATCH --ntasks=16
#SBATCH --nodes=1

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "$SLURM_NTASKS_PER_NODE tasks per node"
echo "Running on nodes: $SLURM_NODELIST"
echo "------------------------------------------------------------"

module purge
module load intel/2018.3
module load totalview/2017.1.21

export I_MPI_FABRICS=shm:ofi
export LD_LIBRARY_PATH=/p/system/packages/libfabric/1.7.1/lib:$LD_LIBRARY_PATH
unset FI_PROVIDER_PATH
export FI_LOG_LEVEL=debug
#export I_MPI_DEBUG=5

## srun version (requires SLURM's process management interface (libpmi) to srun)
export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so
#srun -n $SLURM_NTASKS $PWD/sumprimes 0 100000000
#WORKS tvscript -mpi SLURM -tasks ${SLURM_NTASKS} $PWD/sumprimes -a 0 100000000
tvscript -memory_debugging -mem_detect_use_after_free -mpi SLURM -tasks ${SLURM_NTASKS} $PWD/sumprimes -a 0 100000000
#tvscript -verbosity info -memory_debugging -mpi "SLURM" -np ${SLURM_NTASKS} -starter_args "/home/linstead/cluster-examples/sumprimes/mpi/sumprimes" srun

#mpirun -n $SLURM_NTASKS $HOME/cluster-examples/sumprimes/mpi/sumprimes 0 1000000000
