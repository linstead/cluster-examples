#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "primes.h"

int isprime(int n) {
    double sqrtn; 
    int i;

    if (n < 2) return FALSE;
    else if ((n == 2) || (n == 3)) return TRUE;

    sqrtn = floor(sqrt(n));

    for (i=2; i<=sqrtn; i++) {
        if ((n % i) == 0) {
            return FALSE;
        }
    }

    return TRUE;
}

unsigned long long int sumprimes(int lower, int upper) {
    unsigned long long int sum = 0;
    int np = 0;
    int i;

    for (i=lower; i<=upper; i++) {
        if (isprime(i)) {
            sum += i;
            np++;
        }
    }

    printf("Number of primes: %d\n", np);

    return sum;
}

