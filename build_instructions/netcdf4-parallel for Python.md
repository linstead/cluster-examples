1) Download a version from Unidata: https://github.com/Unidata/netcdf4-python/releases
   
   In this case, 1.5.1.2, and unpack.
   
2) Create a conda environment, based on Intel, with mpi4py and numpy
    - `module load anaconda/5.0.0_py3`
    - `conda create -n par_io -c intel mpi4py numpy`
    
3) activate: `source activate par_io`

4) load an Intel module (for the compiler)
    - `module load intel/2018.3`
    
5) load a recent parallel NetCDF4 module and HDF5 module
    - `module load netcdf-c/4.6.2/intel/parallel`
    - `module load hdf5/1.10.2/intel/parallel`
    
6) in the unpacked netcdf4-parallel directory from step 1:
    - `CC=mpiicc python setup.py install`
    
7) confirm module installed:
    - `conda list | grep netcdf4`
    
    ```
      netcdf4                   1.5.1.2                  pypi_0    pypi
    ```

To use:
    `module load anaconda/5.0.0_py3`
    `source activate par_io`
    
To test:
    `export I_MPI_FABRICS=shm:shm` # only to be set for testing on login nodes, not for submitted jobs
    `python -c "from netCDF4 import Dataset; Dataset('test.nc', 'w', parallel=True)"`