import sys
import datetime
from time import sleep

if (len(sys.argv) <= 2):
    delay = 0
else:
    delay = int(sys.argv[2])

t1 = datetime.datetime.now()

with open(sys.argv[1]) as f:
    sum=0
    elapsed = (datetime.datetime.now()-t1).total_seconds()
    while ((elapsed < delay) or (delay==0)):
        data = f.read()

        for i in data:
            sum += ord(i)

        if (delay==0): break # not waiting for elapsed time to pass

        t2 = datetime.datetime.now()
        elapsed = (t2-t1).total_seconds()

print(sys.argv[1], sum)

