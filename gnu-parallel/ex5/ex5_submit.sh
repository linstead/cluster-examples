#!/bin/sh

# The current PIK cluster's SSH configuration disallows direct SSH between nodes, so this
# example is non-functional.

#SBATCH --time=00:05:00
#SBATCH --job-name=gnu_parallel_ex5
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --partition=standard
#SBATCH --account=its # replace with your project name
#SBATCH --nodes=2
#SBATCH --tasks-per-node=16

module load anaconda/5.0.0_py3
module load parallel

srun="srun --exclusive -N1 -n1"

scontrol show hostnames $SLURM_JOB_NODELIST > nodefile

parallel="parallel -j16 --sshloginfile nodefile -vv "

find $PTMP/tmpdata/ -type f | $parallel "$srun python ../scripts/process_file.py {} 5"

rm nodefile
