#!/bin/bash

module load parallel
module load anaconda/5.0.0_py3

find $PTMP/tmpdata/ -type f | parallel -j8 --bar --joblog tasks.log --resume python ../scripts/process_file.py {} 10
