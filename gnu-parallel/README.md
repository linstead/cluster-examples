# GNU Parallel and SLURM

## Getting Started

[GNU Parallel](https://www.gnu.org/software/parallel/) can be used to execute
shell jobs in parallel using one or more CPU cores or computers.  It is also
capable of being used via SLURM. A typical workload might be handling the pre-
or post-processing of a large set of data files.

`parallel` handles the splitting of the workload across the resources available
to it, and also includes logging/resume capability so you can restart partially
completed processing.

On the PIK cluster, load the latest version of `parallel`:

```bash
module load parallel
```

Here's a really simple example, just printing (with `echo`) a list of letters:

```bash
parallel "echo {}" ::: A B C D
```

To see behind the scenes, here's another simple example. Enter these commands
in one shell:

```bash
function doit() { sleep 30; echo $1; }
export -f doit
parallel -j 4 doit {} ::: A B C D
```

In a new shell, enter this to list the processes started by parallel:
```bash
ps -aef | grep [d]oit
```

Using `-j 4` will run 4 parallel jobs, reflected in the output of `ps`:

```bash
linstead 18171 18147  0 12:47 pts/2    00:00:00 /bin/bash -c doit A
linstead 18174 18147  0 12:47 pts/2    00:00:00 /bin/bash -c doit B
linstead 18177 18147  0 12:47 pts/2    00:00:00 /bin/bash -c doit C
linstead 18179 18147  0 12:47 pts/2    00:00:00 /bin/bash -c doit D
```

If, in contrast, you run `parallel` with `-j 1`, you will effectively be
running each process in series:

```bash
linstead 19451 19428  0 12:51 pts/2    00:00:00 /bin/bash -c doit A
```

The first example will run in a little over 30 seconds (the time spent in
`sleep`), since all 4 `sleep`s happen in parallel on different CPUs. The second
will need a little over 2 minutes to run, because 1 CPU has to do all the
sleeping!

By default, without `-j` set, `parallel` will use as many CPU cores as
required, up to __all CPU cores on the computer__. Please bear this in mind on
shared resources, like a cluster login node.


## Examples Part I - Using `parallel` interactively

The examples mentioned here are in the numbered subdirectories of this repository.

### Example 1 : Creating randomly named files with a small amount of data in each

Creating the files:
```bash
(for i in {1..10}; do mktemp -u -p.; done) | parallel -j2 touch ./{1}
```

Adding data to the files

```bash
ls tmp.* | parallel -j4 'echo $(uuidgen) > {1}'
```




Run [ex1_generate_files.sh](ex1/ex1_generate_files.sh) thus:
```bash
./ex1_generate_files.sh 100
```

This script uses `parallel` to create a number of files in your /p/tmp
directory, each with a random filename (beginning with `tmp.`) and a small
amount of random data in each Omitting the command line argument (100) will
default to 10 files. This script uses 4 CPU cores.

The core of the script runs `parallel` like this:

```bash
for i in $(seq $COUNT); do generate_filename; done | parallel --link -j4 -a <(seq $COUNT | xargs -I{} uuidgen) -a - echo {1} ">" $DATADIR/{2} 
```

A Bash function (`generate_filename`) is run `$COUNT` times, with the output
being piped into the call to `parallel`. For each filename generated,
`parallel` takes one set of data (generated with Bash process substitution
(`<()`) and `uuidgen`) and writes it to the filename under `$DATADIR`. The two
data sources are given with the `-a` arguments, and referenced by the
positional replacement strings `{1}` and `{2}`. `--link` tells `parallel` to
take one item from each input source.

### Example 2 : Processing files with a Bash script (find . * | parallel)

A simple Python program ([process_file.py](scripts/process_file.py)) sums the
ASCII value of all the characters in a given file. We can apply this to all the
files created in the previous step using a straightforward Bash for-loop thus:

```bash
for i in $PTMP/tmpdata/*; do python process_file.py $i; done
```

or using by `xargs`:

```bash
find $PTMP/tmpdata/ -type f -print | xargs -I'{}' python process_file.py '{}'
```

Both of these take about 3 seconds to process 100 files. [Note that `xargs` can
also be used in parallel mode with the `-P/--max-procs` flag.]

However, we can parallelise this onto, for example, 8 CPUs (see
[ex2_process_files.sh](ex2/ex2_process_files.sh))

```bash
find $PTMP/tmpdata/ -type f | parallel -j8 python process_file.py {}
```

This takes less than half a second to process 100 files.

### Example 3 : Logging and restarting interrupted runs

`parallel` has two parameters for controlling logging and restarting of runs,
`--joblog` and `--resume`. With these, one can save the state of a parallelised
run and in case of interruptions, `parallel` will pick up where it left off
without repeating successful runs. It also repeats partial runs.

[ex3_process_file.sh](ex3/ex3_process_files.sh) demonstrates this:

```bash
find $PTMP/tmpdata/ -type f | parallel -j8 --joblog tasks.log --resume python process_file.py {} 10
```

The second parameter to process_file.py (`10`) adds a delay to each run, to
give you time to cancel (`Ctrl-c`) or kill (`kill -9`) a run and see how
`parallel` handles it.

You can also see the progress by modifying the `parallel` command, adding the
parameter `--bar`. Notice that at 16%, the run was cancelled and restarted.
`parallel` picked up at 16% though.

```bash
0% 0:100=0s /p/tmp/linstead/tmpdata/1ad72                                                                                                                                                     2403
1% 1:99=0s /p/tmp/linstead/tmpdata/d506e                                                                                                                                                      2297
2% 2:98=0s /p/tmp/linstead/tmpdata/9fe0b                                                                                                                                                      2160
3% 3:97=0s /p/tmp/linstead/tmpdata/ccbf8                                                                                                                                                      2489
.
<snip>
.
13% 13:87=54s /p/tmp/linstead/tmpdata/2d375                                                                                                                                                   2437
14% 14:86=54s /p/tmp/linstead/tmpdata/6b378                                                                                                                                                   2390
15% 15:85=54s /p/tmp/linstead/tmpdata/3caa1                                                                                                                                                   2394
16% 16:84=54s /p/tmp/linstead/tmpdata/a6a82                                                                                                                                                   
^C
$ ./ex3_process_file.sh 
16% 16:84=24s /p/tmp/linstead/tmpdata/a6a82 
```

To restart an entire run from the beginning, you need to either delete the log
file (in this example `tasks.log`, or supply a new name)

## Examples Part II - via SLURM

Using parallel in an interactive session on a cluster login node (as above) is
a good way to test syntax and arguments. It's not recommended to perform
long-running or resource-intensive work this way, since the cluster login nodes
are a shared resource and overloading them will have adverse effects on your
(and your colleagues) work.

Using the SLURM job scheduling software will give you access to your own
dedicated cluster resources. User guides for SLURM can be found
[here](https://www.pik-potsdam.de/services/it/hpc/user-guides), the rest of
this example assumes you are familiar with it's operation.

### Example 4 : using --ntasks and --nodes

This SLURM job requests an allocation of 8 tasks. We need to specify
`--nodes=1` to ensure that all tasks are run on the same node.

```bash
sbatch ex4_submit.sh
```

If the program you're running is itself multithreaded (e.g. uses Python's
numpy, or OpenMP threads), you'll need to add a --cpus-per-task option.

Shortcut: you can replace `--ntasks=16` with `--exclusive` to get the full 16
CPUs of a standard compute node. For the Broadwell partition, this is 32 CPU
cores.

### Example 5 : Running on more than one node.

The current node configuration at PIK disallows direct SSH connections between
compute servers. For this reason, the `--sshloginfile` feature of `parallel` 
does not work. Example 5, however, demonstrates in principle how this would
work on another SLURM-based machine.

## GNU Parallel vs. SLURM job arrays

[A brief introduction to SLURM job arrays can be found in the
[`jobarrays`](jobarrays) subdirectory of this repository]

GNU Parallel and SLURM job arrays offer very similar capabilities, in that they
parallelise (or more correctly, distribute) compute jobs across available
resources. However, job arrays require more internal logic in your workflow to
translate SLURM_ARRAY_JOB_ID into a set of parameters for your code. GNU
Parallel allows greater flexibility in the parameters being passed to the
parallelisation code, see the example of finding and processing multiple
filenames.

## References

1. GNU Parallel Homepage: https://www.gnu.org/software/parallel/
2. `man 1 parallel`
3. `man 7 parallel_tutorial`
4. https://rcc.uchicago.edu/docs/tutorials/kicp-tutorials/running-jobs.html#gnu-parallel



