#!/bin/bash

module load parallel

### 
# Set up the data location
###

DATADIR=$PTMP/tmpdata

if [ ! -d $DATADIR ]
then
    mkdir $DATADIR
fi

###
# Set default number of files, unless specified
###

[[ -z $1 ]] && COUNT=10 || COUNT=$1

###
# Use a function to generate filenames, for clarity.
###

function generate_filename() {
   mktemp -u -p.
}
export -f generate_filename

###
# Create files with a little random content
###

# The --link option makes parallel read one argument from each of the input sources. Compare these two:
# parallel echo {1} {2} ::: 1 2 3 ::: a b c
# parallel --link echo {1} {2} ::: 1 2 3 ::: a b c

# See man 1 parallel (section '::: arguments') for an explanation of the '-a' parts.

for i in $(seq $COUNT); do generate_filename; done | parallel --link -j4 -a <(seq $COUNT | xargs -I{} uuidgen) -a - echo {1} ">" $DATADIR/{2}
