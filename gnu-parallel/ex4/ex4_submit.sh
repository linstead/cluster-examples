#!/bin/sh

#SBATCH --time=00:05:00
#SBATCH --job-name=gnu_parallel_ex4
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --partition=standard
#SBATCH --account=its # replace with your project name
#SBATCH --nodes=1
#SBATCH --ntasks=8

module load anaconda/5.0.0_py3
module load parallel

srun="srun --exclusive -N1 -n1"

# -j is the number of tasks parallel runs so we set it to $SLURM_NTASKS
# Note that --ntasks=1 and --cpus-per-task=8 will have srun start one copy of the program at a time.

find $PTMP/tmpdata/ -type f | parallel -j $SLURM_NTASKS "$srun python ../scripts/process_file.py {} 5"

