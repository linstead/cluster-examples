#!/bin/bash

module load parallel
module load anaconda/5.0.0_py3

DATA=$PTMP/tmpdata2
if [ ! -d $DATA ]
then
    echo "$DATA not found; have you run Exercise 1?"
else
    find $DATA -type f | parallel -j8 python ../scripts/process_file.py {}
fi

