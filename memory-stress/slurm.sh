#!/bin/bash

#SBATCH --qos=priority
#SBATCH --partition=priority
#SBATCH --job-name=mem
#SBATCH --nodes=1
#SBATCH --ntasks=15
##SBATCH --cpus-per-task=1
##SBATCH --exclusive
#SBATCH --output=memory-%j.out
#SBATCH --error=memory-%j.err
#SBATCH --account=its
#SBATCH --time=00:15:00

##SBATCH --mem-per-cpu=512

#module load intel/2018.3
source /p/system/packages/intel/oneapi/2021.1.1/setvars.sh
export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so

unset I_MPI_DAPL_UD_PROVIDER
unset I_MPI_DAPL_UD
export I_MPI_FABRICS=shm:ofi
export FI_PROVIDER=mlx

# exhaust the memory
#srun -n $SLURM_NTASKS $PWD/himem
#srun -n 1 $PWD/himem
#srun -n 1 $PWD/allocmem 3700000000

# go no higher than 3.25GB on all tasks, nodes will crash (see --ntasks above)
#srun -l -n $SLURM_NTASKS $PWD/allocmem_mpi 3.25 3.25
srun -l -n $SLURM_NTASKS $PWD/allocmem_mpi 3.25 3.9
#srun --kill-on-bad-exit=0 -n $SLURM_NTASKS $PWD/himem_mpi

# simply coredump
#srun -n $SLURM_NTASKS $PWD/dumpcore
#$PWD/coredump
