#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

int
main(int argc, char *argv[]) {

    int *data, *tmp;
    unsigned int chunksize = 20000000; // number of array elements per chunk
    int chunks = 1;
    size_t nbytes;
    int size, rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    // Set up an array for data.
    nbytes = chunks * chunksize * sizeof(*data);

    /* Note: 
     * Linux memory overcommitment generally means that malloc()/realloc() etc never fail to
     * return a valid pointer, so we won't be able to handle 
     */
    data = (int *) malloc (nbytes);

    if (NULL == data) {
        printf("cannot malloc %ld bytes\n", chunks * chunksize * sizeof(*data));
        return -1;
    }

    while (1) {

        fflush(stdout);

        ++chunks;
        nbytes = chunks * chunksize * sizeof(*data);

        printf("Rank %d: attempt to realloc: %u MB (%d chunks)\n", rank, nbytes/1024/1024, chunks);
        tmp = realloc (data, nbytes);

        if (tmp == NULL) {
            printf ("Cannot realloc() %u MB for chunk %d of data array!\n", nbytes/1024/1024, chunks);
            break;
        } else {
            data = tmp;
        }

        // only on access will a memory error be generated:
        memset(data, 0, nbytes);

    }
        
    free(data);

    MPI_Finalize();
    return 0;
}
