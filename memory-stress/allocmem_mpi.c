#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <mpi.h>

int
main(int argc, char *argv[]) {

    int *data;
    size_t nbytes, xnbytes;
    float nbytes_gb, xnbytes_gb;

    int size, rank;

    /* check that we have enough command line arguments */
    if (argc == 2) {
        nbytes_gb = atof(argv[1]);
        xnbytes_gb = 8; // too big for single core memory allowance
    } else if (argc == 3) {
        nbytes_gb = atof(argv[1]);
        xnbytes_gb = atof(argv[2]);
    } else {
        exit(1);
    }

    nbytes = (size_t) (nbytes_gb * 1024 * 1024 * 1024);
    xnbytes = (size_t) (xnbytes_gb * 1024 * 1024 * 1024);

    MPI_Init(&argc, &argv);
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    data = (int *) malloc (nbytes);

    if (rank == 3) {
        nbytes = xnbytes;
    }

    if (NULL == data) {
        printf("Cannot malloc %5.2f GB\n", nbytes_gb);
        return -1;
    }

    fflush(stdout);

    // only on access will a memory error be generated:
    //mlock(data, nbytes);

    // initialise the data array
    for (int i = 0; i < nbytes/sizeof(*data); i++) {
        *(data+i) = i;
    }

    if (rank == 3) {
        printf("locked %5.2f GB\n", xnbytes_gb);
    } else {
        printf("locked %5.2f GB\n", nbytes_gb);
    }

    free(data);
    MPI_Finalize();
    return 0;
}
