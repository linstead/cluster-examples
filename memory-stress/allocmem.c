#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>

int
main(int argc, char *argv[]) {

    int *data;
    size_t nbytes;

    /* check that we have enough command line arguments */
    if (argc == 2) {
        nbytes = atoll(argv[1]);
    } else {
        exit(1);
    }

    data = (int *) malloc (nbytes);

    if (NULL == data) {
        printf("cannot malloc %zu bytes (%zu GB)\n", nbytes, nbytes/1024/1024/1024);
        return -1;
    }

    fflush(stdout);

    printf("attempt to lock: %zu MB...", nbytes/1024/1024);

    // only on access will a memory error be generated:
    mlock(data, nbytes);

    printf("...done\n");

    free(data);
    return 0;
}
