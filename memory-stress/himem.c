#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>

int
main(int argc, char *argv[]) {

    int *data, *tmp;
    unsigned int chunksize = 10000000; // number of array elements per chunk
    long chunks = 1;
    //unsigned long long int nbytes;
    size_t nbytes;

    // Set up an array for data.
    nbytes = chunks * chunksize * sizeof(*data);

    /* Note: 
     * Linux memory overcommitment generally means that malloc()/realloc() etc never fail to
     * return a valid pointer, so we won't be able to handle 
     */
    data = (int *) malloc (nbytes);

    if (NULL == data) {
        printf("cannot malloc %ld bytes\n", chunks * chunksize * sizeof(*data));
        return -1;
    }

    while (1) {

        fflush(stdout);

        ++chunks;
        nbytes = chunks * chunksize * sizeof(*data);

        printf("attempt to realloc: %zu MB (%ld chunks)\n", nbytes/1024/1024, chunks);
        tmp = realloc (data, nbytes);

        if (tmp == NULL) {
            printf ("Cannot realloc() %zu MB for chunk %d of data array!\n", nbytes/1024/1024, chunks);
            break;
        } else {
            data = tmp;
        }

        // only on access will a memory error be generated:
        mlock(data, nbytes);

        // initialise the data array
        //for (int i = 0; i < (chunks * chunksize); i++) {
        //    *(data+i) = i;
        //}

    }
        
    free(data);
    return 0;
}
