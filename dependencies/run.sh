#!/bin/bash

# Run a series of SLURM jobs in a chain of dependencies
# We'll submit slurm1.sh, slurm2.sh and slurm3.sh with sbatch

# submit slurm1.sh as the initial job, extract the job ID
job1=$(sbatch --parsable slurm1.sh)

# only start slurm2 if slurm1 has completed with an exit code of 0
job2=$(sbatch --parsable --dependency=afterok:$job1 slurm2.sh)

# slurm3.sh already sets its own "BeginTime", but we can also override it here:
job3=$(sbatch --parsable --dependency=afterok:$job2 --begin=16:45 slurm3.sh)

# See "man sbatch" for a description of after:, afterok:, afterany: etc.
