This example shows how to run multiple Slurm jobs as a chain of dependencies.
The script "run.sh" is the main controller for the other, separate, job scripts (slurm1-3.sh)
