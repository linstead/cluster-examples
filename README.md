# Cluster Examples

This is a collection of (mostly) short programs and scripts demonstrating different ways of using the PIK cluster.

Let us know if you'd like new examples (or have found a bug) via the GitLab Issue Tracker.

Here's a brief description of some of the more interesting examples:


- 00_Getting_Started
    
    a series of simple examples demonstrating how to prepare and submit jobs via Slurm.

- jupyter
    
    how to run Jupyter notebooks on a cluster compute node, with the front-end on your own laptop

- sumprimes

    demonstrates parallelism via a program for calculating the sum of prime numbers between two bounds. There are three versions (in C): serial, OpenMP (threaded) parallel and MPI (task-based) parallel
    
- dependencies
    
    how to chain together SLURM jobs as a set of dependent jobs

- gnu-parallel
    
    using GNU parallel to parallelise shell scripts (which can run other code too)

- jobarrays
    
    using the SLURM arrays feature for sets of similar jobs



