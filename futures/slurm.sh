#!/bin/bash

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --output=test-%J.out
##SBATCH --partition=priority
##SBATCH --qos=priority
#SBATCH --time=00:15:00

# To run this code, you need to create a conda env 
# with mpi4py from the Intel channel:
# conda create -n mpi_py3 -c intel mpi4py

rm -f julia.pgm

module load anaconda/5.0.0_py3
source activate mpi_py3

export I_MPI_DEBUG=10

# For now, we need to manually set paths to libfabric (the default fabric 
# provider for Intel 2019 onwards)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/p/tmp/linstead/envs/mpi_py3/lib/libfabric
export FI_PROVIDER_PATH=/p/tmp/linstead/envs/mpi_py3/lib/libfabric/prov
export I_MPI_FABRICS=shm:ofi # shm:dapl not applicable for libfabric

#export FI_INFO=debug

# The following two line are not needed for Intel (impi_rt/intelpython) 2019.3
export FI_VERBS_IFACE=ib0
export I_MPI_OFI_PROVIDER=verbs

# in case these were set:
unset I_MPI_DAPL_UD
unset I_MPI_DAPL_UD_PROVIDER

export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so
srun -n$SLURM_NTASKS python -m mpi4py.futures julia.py

# NOTES:
# https://software.intel.com/zh-cn/node/783815
